DROP TABLE IF EXISTS `user`;


CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
);


CREATE TABLE `Dog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `breedId` int(100) NOT NULL,
  `bornDate` date DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilephoto` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dogName` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `breed_id` (`breedId`),
  KEY `ID` (`ID`),
  KEY `location_id` (`locationId`),
  KEY `userid` (`userid`),
  CONSTRAINT `Dog_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `User` (`ID`),
  CONSTRAINT `fk_dog_breed_id` FOREIGN KEY (`breedId`) REFERENCES `Breed` (`ID`),
  CONSTRAINT `fk_dog_location_id` FOREIGN KEY (`locationId`) REFERENCES `States` (`ID`)
);


CREATE TABLE `Breed` (
  `ID` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
);

CREATE TABLE `States` (
  `ID` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `fk_states_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `States` (`ID`)
);