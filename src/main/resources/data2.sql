
INSERT INTO `User` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO `user` VALUES
    (1,'Arco Vara','https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC','1994-07-04',20,3640000.00,-540000.00,8998367,1.09,0.01),
    (2,'Baltika','https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT','1997-05-09',946,44690000.00,-5120000.00,4079485,0.32,NULL),
    (3,'Ekspress Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=EEG','1995-06-21',1698,60490000.00,10000.00,29796841,0.84,NULL),
    (4,'Harju Elekter','https://www.nasdaqbaltic.com/market/logo.php?issuer=HAE','1996-05-14',744,120800000.00,1550000.00,17739880,4.25,0.18),
    (5,'LHV Group','https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV','2005-01-25',366,64540000.00,25240000.00,26016485,11.75,0.21),
    (6,'Merko Ehitus','https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK','1990-11-05',740,418010000.00,19340000.00,17700000,9.30,1.00),
    (7,'Nordecon','https://www.nasdaqbaltic.com/market/logo.php?issuer=NCN','1998-01-01',662,223500000.00,3380000.00,32375483,1.04,0.12),
    (8,'Pro Kapital Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=PKG','1994-01-01',91,27990000.00,16830000.00,56687954,1.43,NULL),
    (9,'Tallink Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL','1997-08-21',7201,949720000.00,40050000.00,669882040,0.96,0.12),
    (10,'Tallinna Kaubamaja Grupp','https://www.nasdaqbaltic.com/market/logo.php?issuer=TKM','1960-07-21',4293,681180000.00,30440000.00,40729200,8.36,0.71),
    (11,'Tallinna Sadam','https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM','1991-12-25',468,130640000.00,24420000.00,263000000,1.96,0.13),
    (12,'Tallinna Vesi','https://www.nasdaqbaltic.com/market/logo.php?issuer=TVE','1967-01-01',314,62780000.00,24150000.00,20000000,10.80,0.75);

