package com.valiit.PetProjectAPI.repository; //Need on pöördumised andmebaasi - BE esimene samm

import com.valiit.PetProjectAPI.model.Dog;
import com.valiit.PetProjectAPI.model.Playground;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PetsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

/* Päringud kuvamise jaoks  näita kõiki, näita ühte inimese koeri, näita koera id järgi */

    public List<Dog> getDogs() {
        return jdbcTemplate.query("select d.*, b.name AS breedname from Dog d INNER JOIN Breed b ON b.ID=d.breedId", mapDogRows);
    }

      public List<Dog> getDogs(int id) {
        return jdbcTemplate.query("select d.*, b.name AS breedname from Dog d INNER JOIN Breed b ON b.ID=d.breedId where d.userid = ?", new Object[]{id}, mapDogRows);
    }


    public Dog getDog(int id) {
        List<Dog> dogs = jdbcTemplate.query("select d.*, b.name AS breedname from Dog d INNER JOIN Breed b ON b.ID=d.breedId where d.ID = ? ", new Object[]{id}, mapDogRows);
        return dogs.size() > 0 ? dogs.get(0) : null;
    }


    public boolean dogExists(Dog dog) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from Dog where dogName = ?",
                new Object[]{dog.getDogName()}, Integer.class);
        return count != null && count > 0;
    }

    public void addDog(Dog dog) {
        jdbcTemplate.update(
                "insert into Dog (dogName, breedId, bornDate, locationId, description, profilephoto, userid, isdeleted, size, female) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", dog.getDogName(), dog.getBreedId(), dog.getBornDate(),
                dog.getLocationId(), dog.getDescription(), dog.getProfilePhoto(), dog.getUserId(), dog.getisIsdeleted(), dog.getSize(), dog.isFemale()
        );
    }

    public void updateDog(Dog dog) {
            jdbcTemplate.update(
                "update Dog set dogName = ?, breedId = ?, bornDate = ?, locationId = ?, description = ?, " +
                        "profilephoto = ?, userid = ?, isdeleted = ?, size = ?, female = ? where ID = ?",
                dog.getDogName(), dog.getBreedId(), dog.getBornDate(), dog.getLocationId(), dog.getDescription(),
                dog.getProfilePhoto(), dog.getUserId(), dog.getisIsdeleted(), dog.getSize(), dog.isFemale(), dog.getId()
        );
    }

    public List<Playground> getAllPlaygrounds() {
        return jdbcTemplate.query("select * from States", mapStateRows);
    }

    public List<Playground> getPlaygrounds(int dogId) {
        return jdbcTemplate.query("select States.* from States INNER JOIN Dogstates ON States.ID=Dogstates.statesid where Dogstates.dogid = ?", new Object[]{dogId}, mapStateRows);
    }


    public void deleteDog(int id) {
        jdbcTemplate.update("delete from Dog where id = ?", id);
    }

    private RowMapper<Dog> mapDogRows = (rs, rowNum) -> {
        Dog dog = new Dog();
        dog.setId(rs.getInt("id"));
        dog.setDogName(rs.getString("dogName"));
        dog.setBreedId(rs.getInt("breedId"));
        dog.setBreedName(rs.getString("breedname"));
        dog.setBornDate(rs.getDate("bornDate") != null ? rs.getDate("bornDate").toLocalDate() : null);
        dog.setLocationId(rs.getInt("locationId"));
        dog.setDescription(rs.getString("description"));
        dog.setProfilePhoto(rs.getString("profilePhoto"));
        dog.setIsdeleted(rs.getBoolean("isdeleted"));
        dog.setSize(rs.getString("size"));
        dog.setFemale(rs.getBoolean("female"));
        return dog;
    };

    private RowMapper<Playground> mapStateRows = (rs, rowNum) -> {
        Playground  playground = new Playground();
        playground.setId(rs.getInt("ID"));
        playground.setLocation(rs.getString("location"));
        playground.setPlayground(rs.getString("playground"));
        playground.setPlaygroundPhoto(rs.getString("playgroundPhoto"));

        return playground;
    };
}
