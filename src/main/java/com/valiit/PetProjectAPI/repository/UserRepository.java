package com.valiit.PetProjectAPI.repository; //Need on pöördumised andmebaasi - BE esimene samm

import com.valiit.PetProjectAPI.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    /* Kasutajaga seotud toimingud lisa, muuda, kustuta, kontrolli kas eksiteerib  */

    public void addUser(User user) {
        jdbcTemplate.update("insert into `User` (`username`, `password`, `owner_name`, `email`, `phonenumber`, `userphoto`, `userlocation`) " +
                "values (?, ?, ?, ?, ?, ?, ?)", user.getUsername(), user.getPassword(), user.getOwnerName(), user.getEmail(), user.getPhoneNumber(), user.getUserphoto(), user.getUserlocation());
    }


    public void updateUser(User user) {
        jdbcTemplate.update(
                "update User set username = ?, password = ?, owner_name = ?, email = ?, phonenumber = ?, userphoto = ?, userlocation = ? where id = ?",
                user.getUsername(), user.getPassword(), user.getOwnerName(), user.getEmail(), user.getPhoneNumber(), user.getUserphoto(), user.getUserlocation(), user.getId()
        );
    }

    public void deleteUser(int id) {
        jdbcTemplate.update("delete from User where id = ?", id);
    }


    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from User where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }


    /* Kuvamisega seotud päringud  näita kõiki, näita ühte*/

    public List<User> getUsers() {
        return jdbcTemplate.query("select * from `User`", mapUserRows);
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query("select *  from `User` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(
                        rs.getInt("ID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("owner_name"),
                        rs.getString("email"),
                        rs.getString("phonenumber"),
                        rs.getString("userphoto"),
                        rs.getString("userlocation")
                ));

        return users.size() > 0 ? users.get(0) : null;
    }

    public User getUsers(int id) {
        List<User> users = jdbcTemplate.query("select * from User where id = ?", new Object[]{id}, mapUserRows);
        return users.size() > 0 ? users.get(0) : null;
    }

    private RowMapper<User> mapUserRows = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setOwnerName(rs.getString("owner_name"));
        user.setEmail(rs.getString("email"));
        user.setPhoneNumber(rs.getString("phonenumber"));
        user.setUserphoto(rs.getString("userphoto"));
        user.setUserlocation(rs.getString("userlocation"));
        return user;
    };

}
