package com.valiit.PetProjectAPI.model;

import java.util.ArrayList;
import java.util.List;

public class User {

    private Integer id;
    private String username;
    private String password;
    private String ownerName;
    private String email;
    private String phoneNumber;
    private String userphoto;
    private List<Dog> userdogs = new ArrayList<>();
    private String userlocation;




    public User() { // VAJALIK SPRINGILE KUNA LOIN OMA KONSTRUKTORI aga sellega ei oska spring midagi teha
    }

    public User(int id, String username, String password, String ownerName, String email, String phonenumber, String userphoto, String userlocation) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.ownerName = ownerName;
        this.email = email;
        this.phoneNumber = phonenumber;
        this.userphoto = userphoto;
        this.userlocation = userlocation;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Dog> getUserdogs() {
        return userdogs;
    }

    public void setUserdogs(List<Dog> userdogs) { this.userdogs = userdogs; }

    public String getUserphoto() { return userphoto; }

    public void setUserphoto(String userphoto) { this.userphoto = userphoto; }

    public String getUserlocation() { return userlocation; }

    public void setUserlocation(String userlocation) { this.userlocation = userlocation; }

}
