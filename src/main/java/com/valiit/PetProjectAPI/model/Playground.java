package com.valiit.PetProjectAPI.model;

public class Playground {
    private Integer id;
    private String location;
    private  String playground;
    private String playgroundPhoto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPlayground() {
        return playground;
    }

    public void setPlayground(String playground) {
        this.playground = playground;
    }

    public String getPlaygroundPhoto() {
        return playgroundPhoto;
    }

    public void setPlaygroundPhoto(String playgroundPhoto) {
        this.playgroundPhoto = playgroundPhoto;
    }
}
