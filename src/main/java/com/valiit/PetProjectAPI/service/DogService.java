package com.valiit.PetProjectAPI.service;

import com.valiit.PetProjectAPI.dto.DogDto;
import com.valiit.PetProjectAPI.dto.PlaygroundDto;
import com.valiit.PetProjectAPI.model.Dog;
import com.valiit.PetProjectAPI.model.Playground;
import com.valiit.PetProjectAPI.model.User;
import com.valiit.PetProjectAPI.repository.PetsRepository;
import com.valiit.PetProjectAPI.repository.UserRepository;
import com.valiit.PetProjectAPI.util.Transformer;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class DogService {

    @Autowired
    private PetsRepository petsRepository;


    @Autowired
    private UserRepository userRepository;

    /* aga see  ei anna mänguväljakuid */
    public List<DogDto> getDogs() {
        List<Dog> dogs = petsRepository.getDogs();
        for (int i = 0; i < dogs.size(); i++){
            List<Playground> playgrounds = petsRepository.getPlaygrounds(dogs.get(i).getId());
            dogs.get(i).setDogplayground(playgrounds);
        }

        return dogs.stream().map(Transformer::toDogDto).collect(Collectors.toList());
    }

    /* see töötab*/

    public DogDto getDog(int id) {
        Assert.isTrue(id > 0, "The ID of the dog not specified");
        Dog dog = petsRepository.getDog(id);
        List<Playground> playgrounds = petsRepository.getPlaygrounds(dog.getId());
        dog.setDogplayground(playgrounds);
        return Transformer.toDogDto(dog);
    }

    public void saveDog(DogDto dogDto) {
        Assert.notNull(dogDto, "Company not specified");
        Assert.hasText(dogDto.getDogName(), "Company name not specified");
        Assert.isTrue(dogDto.getBornDate() == null || dogDto.getBornDate().isBefore(LocalDate.now()),
                "Dog must have been born in the past");

        Dog dog = Transformer.toDogModel(dogDto);
        if (dog.getId() != null && dog.getId() > 0) {
            petsRepository.updateDog(dog);
        } else {
            Assert.isTrue(!petsRepository.dogExists(dog), "The dog with the specified name already exists");
            petsRepository.addDog(dog);
        }
    }

    public void updateDog(DogDto dogDto) {
        Dog dog = Transformer.toDogModel(dogDto);

        String loggedInUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(loggedInUsername);
        dog.setUserId(user.getId());
        if (dog.getId() != null && dog.getId() > 0) {
            petsRepository.updateDog(dog);
        }
    }

    public void deleteDog(int id) {
        if (id > 0) {
            petsRepository.deleteDog(id);
        }
    }

    public List <PlaygroundDto> getPlaygrounds(){
        return petsRepository.getAllPlaygrounds().stream().map(Transformer::toplaygroundDto).collect(Collectors.toList());
    }

}
