package com.valiit.PetProjectAPI.service; // Vahendab repositorylt saadud entity&modeli

import com.valiit.PetProjectAPI.dto.*;
import com.valiit.PetProjectAPI.model.Dog;
import com.valiit.PetProjectAPI.model.Playground;
import com.valiit.PetProjectAPI.model.User;
import com.valiit.PetProjectAPI.repository.PetsRepository;
import com.valiit.PetProjectAPI.repository.UserRepository;
import com.valiit.PetProjectAPI.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PetsRepository petsRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public List<UserRegistrationDto> getUsers() {
        return userRepository.getUsers().stream().map(Transformer::toUserDto).collect(Collectors.toList());
    }

    public User getCurrentUser() {
        String loggedInUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(loggedInUsername);
        List<Dog> dogs = petsRepository.getDogs(user.getId());
        dogs.forEach(dog -> {
            List<Playground> playgrounds = petsRepository.getPlaygrounds(dog.getId());
            dog.setDogplayground(playgrounds);
        });
        user.setUserdogs(dogs);

        return user;
    }


    public UserRegistrationDto getUsers(int id) {
        Assert.isTrue(id > 0, "The ID of the company not specified");
        User user = userRepository.getUsers(id);
        return Transformer.toUserDto(user);
    }

    public void updateUser(UserRegistrationDto userRegistrationDto) {
        User user = Transformer.toUserModel(userRegistrationDto);
        if (user.getId() != null && user.getId() > 0) {
            userRepository.updateUser(user);
        }
    }

    public void deleteUser(int id) {
        if (id > 0) {
            userRepository.deleteUser(id);
        }
    }

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(0, userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()),
                userRegistration.getOwnerName(), userRegistration.getEmail(), userRegistration.getPhoneNumber(), userRegistration.getUserphoto(), userRegistration.getUserlocation());
        if (!userRepository.userExists(userRegistration.getUsername())) {
            userRepository.addUser(user);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }


    /*Autentimisega seotud päringud*/

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}