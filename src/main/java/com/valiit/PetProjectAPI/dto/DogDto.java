package com.valiit.PetProjectAPI.dto;

import com.valiit.PetProjectAPI.model.Playground;
import com.valiit.PetProjectAPI.util.Helper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DogDto {
    private Integer id;
    private String dogName;
    private Integer breedId;
    private LocalDate bornDate;
    private Integer locationId;
    private String description;
    private String profilePhoto;
    private String breedName;
    private String Age;
    private boolean isdeleted;
    private List<PlaygroundDto> dogplayground = new ArrayList<>();
    private boolean female;
    private String size;

    public LocalDate getBornDate() {
        return bornDate;
    }

    public void setBornDate(LocalDate bornDate) {
        this.bornDate = bornDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public Integer getBreedId() {
        return breedId;
    }

    public void setBreedId(Integer breedId) {
        this.breedId = breedId;
    }

    public String getAge() { return Helper.printAge(bornDate) ; }

    /* see setter on katsetamiseks*/

    public void setAge(String age) {
        Age = Helper.printAge(bornDate);
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getBreedName() { return breedName; }

    public void setBreedName(String breedName) { this.breedName = breedName; }

    public boolean getisIsdeleted() { return isdeleted;  }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public boolean isIsdeleted() {
        return isdeleted;
    }

    public List<PlaygroundDto> getDogplayground() {
        return dogplayground;
    }

    public void setDogplayground(List<PlaygroundDto> dogplayground) {
        this.dogplayground = dogplayground;
    }

    public boolean isFemale() {return female; }

    public void setFemale(boolean female) { this.female = female;}

    public String getSize() { return size; }

    public void setSize(String size) { this.size = size; }

}
