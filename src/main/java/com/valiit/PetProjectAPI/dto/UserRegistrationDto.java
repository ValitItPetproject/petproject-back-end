package com.valiit.PetProjectAPI.dto;

import com.valiit.PetProjectAPI.model.User;

import java.awt.*;

public class UserRegistrationDto {
    private Integer id;
    private String username;
    private String password;
    private String ownerName;
    private String email;
    private String phoneNumber;
    private String userphoto;
    private String userlocation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserphoto() { return userphoto;  }

    public void setUserphoto(String userphoto) { this.userphoto = userphoto; }

    public String getUserlocation() {
        return userlocation;
    }

    public void setUserlocation(String userlocation) {
        this.userlocation = userlocation;
    }
}
