package com.valiit.PetProjectAPI.util;

import com.valiit.PetProjectAPI.dto.DogDto;
import com.valiit.PetProjectAPI.dto.PlaygroundDto;
import com.valiit.PetProjectAPI.dto.UserDto;
import com.valiit.PetProjectAPI.dto.UserRegistrationDto;
import com.valiit.PetProjectAPI.model.Dog;
import com.valiit.PetProjectAPI.model.Playground;
import com.valiit.PetProjectAPI.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Transformer {

    public static Dog toDogModel(DogDto initialObject) {
        if (initialObject == null) {
            return null;
        }

        Dog resultingObject = new Dog();
        resultingObject.setId(initialObject.getId());
        resultingObject.setDogName(initialObject.getDogName());
        resultingObject.setBreedId(initialObject.getBreedId());
        resultingObject.setLocationId(initialObject.getLocationId());
        resultingObject.setDescription(initialObject.getDescription());
        resultingObject.setProfilePhoto(initialObject.getProfilePhoto());
        resultingObject.setBreedName(initialObject.getBreedName());
        resultingObject.setIsdeleted(initialObject.getisIsdeleted());
        resultingObject.setBornDate(initialObject.getBornDate());
        resultingObject.setSize(initialObject.getSize());
        resultingObject.setFemale(initialObject.isFemale());
        resultingObject.setDogplayground(initialObject.getDogplayground().stream().map(Transformer::toPlayground).collect(Collectors.toList()));
        return resultingObject;
    }

    public static DogDto toDogDto(Dog initialObject) {
        if (initialObject == null) {
            return null;
        }

        DogDto resultingObject = new DogDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setDogName(initialObject.getDogName());
        resultingObject.setBreedId(initialObject.getBreedId());
        resultingObject.setLocationId(initialObject.getLocationId());
        resultingObject.setDescription(initialObject.getDescription());
        resultingObject.setProfilePhoto(initialObject.getProfilePhoto());
        resultingObject.setBreedName(initialObject.getBreedName());
        resultingObject.setIsdeleted(initialObject.getisIsdeleted());
        resultingObject.setBornDate(initialObject.getBornDate());
        resultingObject.setAge(initialObject.getAge());
        resultingObject.setSize(initialObject.getSize());
        resultingObject.setFemale(initialObject.isFemale());
        resultingObject.setDogplayground(initialObject.getDogplayground().stream().map(p -> {
            return toplaygroundDto(p);
        }).collect(Collectors.toList()));

        return resultingObject;
    }

    public static PlaygroundDto toplaygroundDto(Playground initialObject){
        PlaygroundDto playgroundDto = new PlaygroundDto();
        playgroundDto.setId(initialObject.getId());
        playgroundDto.setLocation(initialObject.getLocation());
        playgroundDto.setPlayground(initialObject.getPlayground());
        playgroundDto.setPlaygroundPhoto(initialObject.getPlaygroundPhoto());
        return playgroundDto;
    }

    public static Playground toPlayground(PlaygroundDto initialObject){
        Playground playground = new Playground();
        playground.setId(initialObject.getId());
        playground.setLocation(initialObject.getLocation());
        playground.setPlayground(initialObject.getPlayground());
        playground.setPlaygroundPhoto(initialObject.getPlaygroundPhoto());
        return playground;
    }


    public static UserRegistrationDto toUserDto(User initialObject) {
        if (initialObject == null) {
            return null;
        }
        UserRegistrationDto resultingObject = new UserRegistrationDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        resultingObject.setPassword(initialObject.getPassword());
        resultingObject.setOwnerName(initialObject.getOwnerName());
        resultingObject.setEmail(initialObject.getEmail());
        resultingObject.setPhoneNumber(initialObject.getPhoneNumber());
        resultingObject.setUserphoto(initialObject.getUserphoto());
        resultingObject.setUserlocation(initialObject.getUserlocation());
        return resultingObject;
    }


    public static User toUserModel(UserRegistrationDto initialObject) {
        if (initialObject == null) {
            return null;
        }
        User resultingObject = new User();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        resultingObject.setPassword(initialObject.getPassword());
        resultingObject.setOwnerName(initialObject.getOwnerName());
        resultingObject.setEmail(initialObject.getEmail());
        resultingObject.setPhoneNumber(initialObject.getPhoneNumber());
        resultingObject.setUserphoto(initialObject.getUserphoto());
        resultingObject.setUserlocation(initialObject.getUserlocation());
        return resultingObject;
    }

    public static UserDto toUserProfileDto(User initialObject) {
        if (initialObject == null) {
            return null;
        }

        UserDto resultingObject = new UserDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        resultingObject.setOwnerName(initialObject.getOwnerName());
        resultingObject.setEmail(initialObject.getEmail());
        resultingObject.setUserlocation(initialObject.getUserlocation());

        List<DogDto> userDogsList = new ArrayList<>();
        for (Dog userdog : initialObject.getUserdogs()) {
            DogDto dogDto = toDogDto(userdog);
            userDogsList.add(dogDto);
        }
        resultingObject.setUserdogs(userDogsList);

        return resultingObject;
    }

}
