package com.valiit.PetProjectAPI.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

public class Helper {

    public static double calculateAge(LocalDate burnDate) {
        LocalDate today = LocalDate.now();
        Period p = Period.between(burnDate, today);
        return p.getYears() ;

    }

    public static String printAge(LocalDate burnDate) {
        LocalDate today = LocalDate.now();
        Period p = Period.between(burnDate, today);
        String agetoprint = (String.valueOf(p.getYears()) + " aastat ja " + String.valueOf(p.getMonths())+ " kuud");
        return agetoprint;
    }


    public void readFile (String inputFile, String table, String column) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String st;

        while ((st = br.readLine()) != null)
            System.out.println(st);
    }

    public static Double round(Double value) {
        return Math.round(value * 100) / 100.0;
    }


    public Integer listSize(List inputList){ //Kas on sellist eraldi vaja??
        return inputList.size();
    }
}
