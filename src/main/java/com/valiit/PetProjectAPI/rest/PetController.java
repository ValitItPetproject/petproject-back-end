package com.valiit.PetProjectAPI.rest; //rest meetodid mida veebist käivtatada

import com.valiit.PetProjectAPI.dto.DogDto;
import com.valiit.PetProjectAPI.dto.PlaygroundDto;
import com.valiit.PetProjectAPI.model.Playground;
import com.valiit.PetProjectAPI.model.User;
import com.valiit.PetProjectAPI.repository.UserRepository;
import com.valiit.PetProjectAPI.service.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dogs")
@CrossOrigin("*")
public class PetController {

    @Autowired
    private DogService dogService;


    @GetMapping
    public List<DogDto> getDog() {
        return dogService.getDogs();
    }

    @GetMapping("playgrounds")
    public List<PlaygroundDto> getPlaygroundsList (){
        return dogService.getPlaygrounds();
    }

    @GetMapping("/{id}")
    public DogDto getDog(@PathVariable("id") int id) {
        return dogService.getDog(id);
    }

    @PostMapping
    public void saveDog(@RequestBody DogDto dogDto) {
        dogService.saveDog(dogDto);
    }

    @PutMapping
    public void updateDog(@RequestBody DogDto dogDto) {
        dogService.updateDog(dogDto);
    }

    @DeleteMapping("/{id}")
    public void deleteDog(@PathVariable("id") int id) {
        dogService.deleteDog(id);
    }

}

