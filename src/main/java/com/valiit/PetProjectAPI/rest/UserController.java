package com.valiit.PetProjectAPI.rest; //rest meetodid mida veebist käivtatada


import com.valiit.PetProjectAPI.dto.*;
import com.valiit.PetProjectAPI.model.User;
import com.valiit.PetProjectAPI.dto.JwtRequestDto;
import com.valiit.PetProjectAPI.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;


    @PutMapping
    public void updateUser(@RequestBody UserRegistrationDto userRegistrationDto) {
        userService.updateUser(userRegistrationDto);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userService.deleteUser(id);
    }

    @GetMapping
    public List<UserRegistrationDto> getUsers() {
        return userService.getUsers();
    }

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @GetMapping("/current")
    public User currentLogin() {
        return userService.getCurrentUser();
    }

}

